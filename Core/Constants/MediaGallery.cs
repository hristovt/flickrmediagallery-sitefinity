﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Core.Constants
{
    public static class MediaGallery
    {
        /// <summary>
        /// Fully qualified type name for the MediaGallery type.
        /// </summary>
        public const string FullTypeName = "Telerik.Sitefinity.DynamicTypes.Model.MediaGalleries.MediaGallery";
        public const string FlickrVideoUrl = "https://www.flickr.com/video_download.gne?id={0}";
        public const string FlickrPhotosetUrl = "https://www.flickr.com/services/rest?method=flickr.photosets.getPhotos&api_key={0}&photoset_id={1}&extras=tags,url_o,media";

            public class MediaGalleryItem
            {
            /// <summary>
            /// Fully qualified type name for the MediaGalleryItem type.
            /// </summary>
            public const string FullTypeName = "Telerik.Sitefinity.DynamicTypes.Model.MediaGalleries.MediaGalleryItem";
            }
    }
}