﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Core.Data
{
    public class MediaModel
    {        
        public string Id { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string VideoUrl { get; set; }
        public string MediaType { get; set; }
        public string Tags { get; set; }
    }
}