﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Core.Data
{
    public class MediaTagModel
    {
        public string Title { get; set; }
        public string Url { get; set; }
    }
}