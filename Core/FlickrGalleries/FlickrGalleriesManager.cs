﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Configuration;
using System.Net.Http;
using System.Xml.Serialization;
using SitefinityWebApp.Core.FlickrGalleries.Model;
using System.Threading.Tasks;
using System.IO;
using System.Web.Http.ExceptionHandling;
using SitefinityWebApp.Core.Constants;

namespace SitefinityWebApp.Core.FlickrGalleries
{
    public static class FlickrGalleriesManager
    {
        public static FlickrModel GetPhotosetItems(string flickrAlbumId)
        {
            string provideUri = string.Format(MediaGallery.FlickrPhotosetUrl, ConfigurationManager.AppSettings.Get("FlickrAPIkey"), flickrAlbumId);

            HttpClient client = new HttpClient();
            var xmlstring = client.GetStreamAsync(provideUri).Result;

            XmlSerializer serializer = new XmlSerializer(typeof(FlickrModel));
            var result = serializer.Deserialize(xmlstring) as FlickrModel;

            return result;
        }
    }
}


