﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace SitefinityWebApp.Core.FlickrGalleries.Model
{
    [XmlRoot("rsp")]
    public class FlickrModel
    {
        [XmlElement("photoset")]
        public List<Photoset> Photosets { get; set; }

        public class Photoset
        {
            [XmlAttribute("id")]
            public string Id { get; set; }
            [XmlAttribute("owner")]
            public string UserId { get; set; }
            [XmlAttribute("title")]
            public string Title { get; set; }
            [XmlElement(ElementName = "photo")]
            public List<Photo> FlickrPhotos { get; set; }
        }

        public class Photo
        {
            [XmlAttribute("id")]
            public string Id { get; set; }
            [XmlAttribute("title")]
            public string Title { get; set; }
            [XmlAttribute("url_o")]
            public string Url { get; set; }
            [XmlAttribute("tags")]
            public List<String> Tags { get; set; }
            [XmlAttribute("media")]
            public string Media { get; set; }
        }
    }
}