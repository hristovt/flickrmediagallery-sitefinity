﻿using Telerik.Sitefinity.Localization;
namespace SitefinityWebApp.CustomResources
{
    
    [ObjectInfo("MediaGalleryResources", ResourceClassId = "MediaGalleryResources", Title = "MediaGalleryResourcesTitle", TitlePlural = "MediaGalleryResourcesTitlePlural", Description = "MediaGalleryResourcesDescription")]
    public class MediaGalleryResources : Resource
    {
        [ResourceEntry(nameof(MediaGalleryResourcesTitle), Value = "Media Gallery Resources", Description = "label: Media Gallery Resources", LastModified = "2020/02/17")]
        public string MediaGalleryResourcesTitle
        {
            get { return this[nameof(this.MediaGalleryResourcesTitle)]; }
        }
        [ResourceEntry(nameof(MediaGalleryResourcesTitlePlural), Value = "Media Gallery Resources", Description = "label: Media Gallery Resources", LastModified = "2020/02/17")]
        public string MediaGalleryResourcesTitlePlural
        {
            get { return this[nameof(this.MediaGalleryResourcesTitlePlural)]; }
        }
        [ResourceEntry(nameof(MediaGalleryResourcesDescription), Value = "Media Gallery Resources", Description = "label: Media Gallery Resources", LastModified = "2020/02/17")]
        public string MediaGalleryResourcesDescription
        {
            get { return this[nameof(this.MediaGalleryResourcesDescription)]; }
        }
        [ResourceEntry(nameof(MediaGalleryResourcesShowAll), Value = "Show all", Description = "label: Show all>", LastModified = "2020/02/17")]
        public string MediaGalleryResourcesShowAll
        {
            get { return this[nameof(this.MediaGalleryResourcesShowAll)]; }
        }
    }
}