﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SitefinityWebApp.Core.Feeds.Model;
using Telerik.Sitefinity.Frontend.DynamicContent.Mvc.Models;
using Telerik.Sitefinity.Frontend.Mvc.Helpers.ViewModels;
using Telerik.Sitefinity.Frontend.Mvc.Models;
using Telerik.Sitefinity.Taxonomies.Model;
using SitefinityWebApp.Core.Data;

namespace SitefinityWebApp.Mvc.Models
{
    public class NewDynamicContentListViewModel : DynamicContentListViewModel
    {      
        public IEnumerable<MediaTagModel> MediaTags { get; set; }
        public IEnumerable<MediaModel> MediaItems { get; set; }       
    }
}