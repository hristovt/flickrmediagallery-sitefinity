﻿using System.Collections.Generic;
using System.Linq;
using SitefinityWebApp.Core.Data;
using SitefinityWebApp.Core.Feeds;
using SitefinityWebApp.Core.Feeds.Model;
using SitefinityWebApp.Core.FlickrGalleries;
using SitefinityWebApp.Core.FlickrGalleries.Model;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.Frontend.DynamicContent.Mvc.Helpers;
using Telerik.Sitefinity.Frontend.DynamicContent.Mvc.Models;
using Telerik.Sitefinity.Frontend.Mvc.Models;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Modules.Libraries;
using Telerik.Sitefinity.Taxonomies.Model;
using SitefinityWebApp.Core.Constants;
using System.Web.Script.Serialization;
using Telerik.Sitefinity.Frontend.Mvc.Helpers;
using Telerik.Sitefinity.Frontend.Mvc.Helpers.ViewModels;
using System;
using Newtonsoft.Json;
using Telerik.Sitefinity.Services;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.ContentLocations;

namespace SitefinityWebApp.Mvc.Models
{
    public class NewDynamicContentModel : DynamicContentModel
    {   

        public string Title { get; set; }
        private List<MediaModel> mediaModel = new List<MediaModel>();
        private List<MediaTagModel> mediaTagModel = new List<MediaTagModel>();

        public override ContentListViewModel CreateListViewModel(ITaxon taxonFilter, int page)
        {
            NewDynamicContentListViewModel viewModel = base.CreateListViewModel(taxonFilter, page) as NewDynamicContentListViewModel;                            

            if (this.ContentType.FullName == MediaGallery.FullTypeName)
            {
                this.mediaModel = new List<MediaModel>();
                this.mediaTagModel = new List<MediaTagModel>();
                foreach (var item in viewModel.Items)
                {
                    this.PopulateMediaGalleryItems(item);
                }

                viewModel.MediaItems = this.mediaModel as IEnumerable<MediaModel>;
                viewModel.MediaTags = this.mediaTagModel.GroupBy(u => u.Url).Select(i => i.First()) as IEnumerable<MediaTagModel>;
            }

            return viewModel;
        }              

        public override ContentDetailsViewModel CreateDetailsViewModel(IDataItem item)
        {
            NewContentDetailsViewModel contentDetailsViewModel = base.CreateDetailsViewModel(item) as NewContentDetailsViewModel;

            contentDetailsViewModel.Title = this.Title;

            return contentDetailsViewModel;
        }

        protected override ContentDetailsViewModel CreateDetailsViewModelInstance()
        {
            return new NewContentDetailsViewModel();            
        }

        private void PopulateMediaGalleryItems(ItemViewModel item)
        {
            if (item.ChildItems("MediaGalleryItems") != null && item.ChildItems("MediaGalleryItems").Any())
            {
                foreach (var childItem in item.ChildItems("MediaGalleryItems"))
                {
                    IList<FlatTaxon> flatTaxons = childItem.GetFlatTaxons("mediatags");
                    string mediaTags = flatTaxons != null && flatTaxons.Any() ? string.Join(" ", flatTaxons.Select(t => t.UrlName)) : string.Empty;
                    this.mediaModel.Add(new MediaModel
                    {
                        ImageUrl = childItem.Fields.Image != null ? MediaContentExtensions.ResolveMediaUrl(childItem.Fields.Image.DataItem) : string.Empty,
                        VideoUrl = childItem.Fields.Video != null ? MediaContentExtensions.ResolveMediaUrl(childItem.Fields.Video.DataItem) : string.Empty,
                        Tags = mediaTags,
                        Title = childItem.Fields.Title
                    });
                    if (flatTaxons != null && flatTaxons.Any())
                    {
                        foreach (var tag in flatTaxons.Distinct())
                        {
                            this.mediaTagModel.Add(new MediaTagModel
                            {
                                Title = tag.Title,
                                Url = tag.UrlName
                            });
                        }
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(item.Fields.FlickrAlbumId))
            {
                this.PopulateFlickrItems(item);
            }
        }

        private void PopulateFlickrItems(ItemViewModel item)
        {
            FlickrModel flickrItems = FlickrGalleriesManager.GetPhotosetItems(item.Fields.FlickrAlbumId);
            var photoset = flickrItems != null && flickrItems.Photosets != null ? flickrItems.Photosets.FirstOrDefault() : null;
            var photos = photoset != null ? photoset.FlickrPhotos : null;
            if (photos != null && photos.Any())
            {
                foreach (var photo in photos)
                {
                    this.mediaModel.Add(new MediaModel
                    {
                        Id = !string.IsNullOrWhiteSpace(photo.Id) ? photo.Id : string.Empty,
                        Title = !string.IsNullOrWhiteSpace(photo.Id) ? photo.Id : string.Empty,
                        ImageUrl = !string.IsNullOrWhiteSpace(photo.Url) ? photo.Url : string.Empty,
                        VideoUrl = photo.Media == "video" ? string.Format(MediaGallery.FlickrVideoUrl, photo.Id) : string.Empty,
                        Tags = photo.Tags != null && photo.Tags.Any() ? string.Join(" ", photo.Tags) : string.Empty,
                        MediaType = photo.Media
                    });
                    if (photo.Tags != null && photo.Tags.Any())
                    {
                        foreach (var tag in photo.Tags)
                        {
                            if (!string.IsNullOrWhiteSpace(tag))
                            {
                                this.mediaTagModel.Add(new MediaTagModel
                                {
                                    Title = tag,
                                    Url = tag
                                });
                            }
                        }
                    }
                }
            }
        }
    }
}